import './App.css';
import AboutUs from './AboutUs';
import {useState} from 'react';
import ListItemContainer from './ListItemContainer/ListItemContainer';


function App() {

const items=[1,2,3,4,5];

const [count,setCount]=useState(0);
const [listItems,setListItems]=useState(items);

  return (
  
      <div> Hi 
  
      <button onClick={hello}>Click</button>
      {/* Passing msg as props to ChildComponent*/}
      <AboutUs msg="Thanks to Know us" passedEvent={updateCounter}/>
      
      
      {/* Passing list as props to ChildComponent*/}
      <AboutUs items={items} passedEvent={updateCounter}/>

      {/*  passing state to child component and event also */}

      <AboutUs countValue={count} passedEvent={updateCounter} /> 

      <h2>Value of Counter {count}  </h2>

      <br/><br/>

        <h4>Click Below on the List to remove element from list of items</h4>

    
    <ListItemContainer numberList={listItems} removeElement={removeElementFromNumberList} /> 



      </div>
      )


function updateCounter()
{
  setCount(count+1);
}

function removeElementFromNumberList(itemToBeRemoved)
{

  alert("Item To Be Removed" +itemToBeRemoved)

  const list=listItems.filter((item)=>
  {
     return item!==itemToBeRemoved;
  })

  setListItems(list)
}





}

function hello()
{
  alert("Hello")
}

export default App;
