import React from "react";
import { shallow } from "enzyme";
import ListItemContainer from "./ListItemContainer";

describe("ListItemContainer", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ListItemContainer />);
    expect(wrapper).toMatchSnapshot();
  });
});
