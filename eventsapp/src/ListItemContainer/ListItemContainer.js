import React, { Component } from "react";

class ListItemContainer extends Component {


  render() {
    return <div>
      
      ListItemContainer
      
      { /*
         <button onClick={()=>this.props.removeElement()} >    Click on this Value {this.props.numberList}</button>
      */}

      {  this.props.numberList.map((item)=>
      {
         return <li key={item.toString()}   onClick={()=>this.props.removeElement(item)}>{item}</li>
      }
      )  }
      
      
      </div>;
  }
}

export default ListItemContainer;
